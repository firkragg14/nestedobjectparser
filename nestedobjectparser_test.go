package nestedobjectparser

import (
	"fmt"
	"testing"
)

func TestNestedObjectParse1(t *testing.T) {
	//{“a”:{“b”:{“c”:”d”}}}
	object := map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{
				"c": "d",
			},
		},
	}

	key := `a/b/c`

	expectedResultValue := "d"

	resultValue := NestedObjectParse(object, key)

	if resultValue != expectedResultValue {
		t.Errorf("NestedObjectParse Failed: Expected result value %v, got %v", expectedResultValue, resultValue)
	} else {
		t.Logf("NestedObjectParse Passed: Expected result value %v, got %v", expectedResultValue, resultValue)
	}
}

func TestNestedObjectParse2(t *testing.T) {
	//{“x”:{“y”:{“z”:”a”}}}
	object := map[string]interface{}{
		"x": map[string]interface{}{
			"y": map[string]interface{}{
				"z": "a",
			},
		},
	}

	key := `x/y/z`

	expectedResultValue := "a"

	resultValue := NestedObjectParse(object, key)

	if resultValue != expectedResultValue {
		t.Errorf("NestedObjectParse Failed: Expected result value %v, got %v", expectedResultValue, resultValue)
	} else {
		t.Logf("NestedObjectParse Passed: Expected result value %v, got %v", expectedResultValue, resultValue)
	}
}

func TestNestedObjectParseEmptyObject(t *testing.T) {
	object := make(map[string]interface{})

	key := `x/y/z`

	expectedResultValue := ""

	resultValue := NestedObjectParse(object, key)

	if resultValue != expectedResultValue {
		t.Errorf("NestedObjectParse Failed: Expected result value %v, got %v", expectedResultValue, resultValue)
	} else {
		t.Logf("NestedObjectParse Passed: Expected result alue %v, got %v", expectedResultValue, resultValue)
	}
}

func TestNestedObjectParseInvalidKey(t *testing.T) {
	//{“a”:{“b”:“c”:”d”}}}
	object := map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{
				"c": "d",
			},
		},
	}

	key := `x/y/z`

	expectedResultValue := ""

	resultValue := NestedObjectParse(object, key)

	if resultValue != expectedResultValue {
		t.Errorf("NestedObjectParse Failed: Expected result value %v, got %v", expectedResultValue, resultValue)
	} else {
		t.Logf("NestedObjectParse Passed: Expected result value %v, got %v", expectedResultValue, resultValue)
	}
}

func TestNestedObjectParsePartialKey(t *testing.T) {
	//{“a”:{“b”:“c”:”d”}}}
	object := map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{
				"c": "d",
			},
		},
	}

	key := `a/b/z`

	expectedResultValue := ""

	resultValue := NestedObjectParse(object, key)

	if resultValue != expectedResultValue {
		t.Errorf("NestedObjectParse Failed: Expected result value %v, got %v", expectedResultValue, resultValue)
	} else {
		t.Logf("NestedObjectParse Passed: Expected result value %v, got %v", expectedResultValue, resultValue)
	}
}

func TestNestedObjectParsePartialKey2(t *testing.T) {
	//{“a”:{“b”:“c”:”d”}}}
	object := map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{
				"c": "d",
			},
		},
	}

	key := `a/b`

	expectedResultValue := ""

	resultValue := NestedObjectParse(object, key)

	fmt.Println(resultValue)

	if resultValue != expectedResultValue {
		t.Errorf("NestedObjectParse Failed: Expected result value %v, got %v", expectedResultValue, resultValue)
	} else {
		t.Logf("NestedObjectParse Passed: Expected result value %v, got %v", expectedResultValue, resultValue)
	}
}

func TestNestedObjectParseNoKey(t *testing.T) {
	//{“a”:{“b”:“c”:”d”}}}
	object := map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{
				"c": "d",
			},
		},
	}

	key := ``

	expectedResultValue := ""

	resultValue := NestedObjectParse(object, key)

	if resultValue != expectedResultValue {
		t.Errorf("NestedObjectParse Failed: Expected result value %v, got %v", expectedResultValue, resultValue)
	} else {
		t.Logf("NestedObjectParse Passed: Expected result value %v, got %v", expectedResultValue, resultValue)
	}
}
