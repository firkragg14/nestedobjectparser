package nestedobjectparser

import (
	"strings"
)

// NestedObjectParse parses a map object for a value based on a key
func NestedObjectParse(object map[string]interface{}, key string) string {
	keys := strings.Split(key, "/")

	currentKey := keys[0]

	keyValue, keyExisted := object[currentKey]

	//key isnt valid so no value to return
	if !keyExisted {
		return ""
	}

	//if the value is a string we have reached the end of the tree
	if stringValue, isString := keyValue.(string); isString {
		return stringValue
	}

	subPath := strings.Join(keys[1:], "/")
	// Now recursively calls the same logic on the next level down until one of the base cases are reached
	return NestedObjectParse(keyValue.(map[string]interface{}), subPath)

}
